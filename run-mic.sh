#!/bin/sh

# drop out on errors
set -e

## Setup ssh
## Based on https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
##
## Install ssh-agent if not already installed, it is required by Docker.
## (change apt-get to yum if you use an RPM-based image)
##
which ssh-agent || ( sudo zypper -n in openssh-clients )

##
## Run ssh-agent (inside the build environment)
##
eval $(ssh-agent -s)

##
## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
## We're using tr to fix line endings which makes ed25519 keys work
## without extra base64 encoding.
## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
##
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

##
## Create the SSH directory and give it the right permissions
##
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# source missing env variables
source tama.env

# get KS from repository
sudo zypper -n in python3-base python3-pip python3-setuptools
sudo python3 -m pip install --upgrade pip
python3 -m pip install --user requests
python3 -m pip install --user bs4

./download_ks.py $REPO droid-config-$DEVICE-ssu-kickstarts
rpm2cpio droid-config-$DEVICE-ssu-kickstarts-*.rpm | cpio -idmv

# setup build env
sudo zypper -n in lvm2 atruncate pigz

sudo zypper rm droid-tools || true # it's ok if you hadn't any
sudo zypper -n in android-tools

sudo mknod /dev/loop0 -m0660 b 7 0
sudo mknod /dev/loop1 -m0660 b 7 1
sudo mknod /dev/loop2 -m0660 b 7 2

echo
cat ./usr/share/kickstarts/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks
echo

# make image
sudo mic create loop --arch=$PORT_ARCH \
    --tokenmap=ARCH:$PORT_ARCH,RELEASE:$TAMA_RELEASE,EXTRA_NAME:$EXTRA_NAME,DEVICEMODEL:$DEVICE \
    --record-pkgs=name,url \
    --outdir=sfe-$DEVICE-$TAMA_RELEASE$EXTRA_NAME \
    ./usr/share/kickstarts/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks

# prepare for sync
mkdir -p $VENDOR/$PLATFORM/$DEVICE
mv sfe-*/*.zip $VENDOR/$PLATFORM/$DEVICE

# sync first and then cleanup the folder
#
# here, we assume that only one file will stay
# in $VENDOR/$PLATFORM/$DEVICE/
rsync -av $VENDOR $SSH_ACCOUNT:.
rsync -av --delete $VENDOR/$PLATFORM/$DEVICE/ $SSH_ACCOUNT:$VENDOR/$PLATFORM/$DEVICE/
