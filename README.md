# Sailfish OS on Sony Xperia Tama devices

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build

[![pipeline status](https://gitlab.com/sailfishos-porters-ci/tama-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/tama-ci/commits/master)
